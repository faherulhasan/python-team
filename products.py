import urllib3
from bs4 import BeautifulSoup
import re
import pyodbc

http = urllib3.PoolManager()
print("Scrapping...")
server=r'EIS-LHR-FAHER\FAHERSQL'
database='scrapper'
user='sa'
pw='admin@123'
con=pyodbc.connect('DRIVER={SQL Server};\
            SERVER='+server+';\
            DATABASE='+database+';\
            UID='+user+';\
            PWD='+pw+';\
            Trusted_Connection=no;')
print("connected")
print('____________URLS______________')
cur=con.cursor()
ips = urllib3.PoolManager()
# Multipages products links
myset = []
lpages=[]
setlist=set()
for ur in cur.execute("select url from scrapper.dbo.resource_locator where webname='Brandsleftover'").fetchall():
    us=ur.url
    # print('Main UR: ',us) 
    myset.append(us)
for multipl in myset:
    print('URL: ',multipl)
    req = ips.request('GET', multipl)
    soups = BeautifulSoup(req.data, 'lxml')
    parentdiv= soups.find_all('div',class_='col-sm-9')
    for prods in parentdiv:
        products=prods.find_all('div',class_='caption')
        for pd in products:
            pname=pd.find('a').get_text()
            price=pd.find('p',class_='price').get_text()
            print('pnames:',pname,'\nPrice:',price)
            cur.execute("INSERT INTO scrapper.dbo.blo_products (productname,price,url) VALUES (?,?,?)",(pname,price,multipl))
            con.commit()
            looppages= soups.find_all('div',class_='col-sm-6 text-left links')
            for lps in looppages:
                for lpagesin in lps.findAll('a', attrs={'href': re.compile("^https://")}):
                    setlist.update([lpagesin.get('href')])
                    

for plpages in setlist:
    print('\nLoop Pages: ', plpages)
    req = ips.request('GET', plpages)
    soups = BeautifulSoup(req.data, 'lxml')
    parentdiv= soups.find_all('div',class_='col-sm-9')
    for prods in parentdiv:
        products=prods.find_all('div',class_='caption')
        for pd in products:
            pname=pd.find('a').get_text()
            price=pd.find('p',class_='price').get_text()
            print('pnames:',pname,'\nPrice:',price)
            cur.execute("INSERT INTO scrapper.dbo.blo_products (productname,price,url) VALUES (?,?,?)",(pname,price,plpages))
            con.commit()
con.close()
